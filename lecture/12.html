<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <script src="../dist/bundle.js"></script>
    <link rel="stylesheet" href="../style/lecture.css">
    <title>Lecture 12: CS 320, Summer 2022</title>
  </head>

  <body>

    <h1>CS 320, Summer 2022</h1>
    <h2>Lecture 12: Survey of modern PL</h2>

    <h3>Introduction</h3>

      <p>In the previous lectures, we surveyed the most influential <i>language paradigms</i> throughout the history of PL.</p>

      <p>Now, we'll turn our attention to the present and near future.</p>

      <p>How does a software developer interact with a language today?</p>

      <p>What has the field of PL been up to recently?</p>

      <p>What languages do people care about in 2022?</p>

      <p>These topics are all open-ended, so please ask questions and share your experiences!</p>

    <h3>Programming in 2022</h3>

      <p>If you came through PSU's lower division, you know a good bit of C++98.</p>

      <p>I started learning to program in around 2002, when C++98 was relatively new.</p>

      <p>Let's reminisce a little.</p>

      <h4>Things that have stayed the same</h4>

        <p>In some ways, programming today is very similar to programming 20 years ago.</p>

        <ul>
          <li>The x86 architecture still dominates the market.</li>
          <li>Even new popular architecures like ARM64 are still fundamentally imperative.</li>
          <li>The procedural and object-oriented paradigms are still the most common.</li>
          <li>C, C++, Java, and Python are still widely used.</p>
        </ul>

        <p>These bullet points are misleading, though!</p>

      <h4>Things that have changed</h4>

        <p>All of these are still relevant, but they've each changed significantly since 2002.</p>

        <ul>
          <li>Multi-core CPUs are standard, and CPU caches work much differently than they used to.</li>
          <li>FPGAs and quantum computing technology offer fundamentally new architectures to target.</li>
          <li>Nearly all languages are multi-paradigm, and include functional features.</li>
          <li>Several newer languages have gained significant popularity.</li>
          <li>C has had two major updates: 11 and 17, with 2x in progress.</li>
          <li>C++ has had <b>five</b> major updates: C++03, 11, 14, 17, and 20.</li>
          <li>Java has had <b>thirteen</b> major updates, from version 5 to version 18.</li>
          <li>Python 3 was released and has had <b>ten</b> major updates, to version 3.10.</li>
        </ul>

        <p>More generally, many of our priorities have changed since 2002!</p>

        <ul>
          <li>Minimizing power usage is a primary goal.</li>
          <li>Smartphones and fast Internet access are common. "App stores" are a whole thing.</li>
          <li>The World Wide Web is a major platform for application development.</li>
        </ul>

        <p>Today, programmers need a different set of core knowledge than they did 20 years ago.</p>

      <h4>Things that have become less important</h4>

        <p>In some ways, programming today is <b>easier</b> than programming in 2002.</p>

        <ul>
          <li>Most languages have well-maintained open-source libraries for most common needs.</li>
          <li>High-level garbage-collected languages are "fast enough" for most use cases.</li>
          <li>For high-performance use cases, modern low-level languages are significantly <i>safer</i> than C++98.</li>
        </ul>

        <p>This means some core knowledge is <b>less</b> important than it used to be.</p>

        <ul>
          <li>Most programmers will never need to implement common data structures or algorithms.</li>
          <li>Most programmers will never need to use things like <span class="code">malloc</span> and <span class="code">delete</span>.</li>
          <li>For the programmers who do need to do these things, there are good tools to help do them <i>correctly</i>.</li>
        </ul>

      <h4>Things that have become more important</h4>

        <p>In some ways, programming today is <b>harder</b> than programming in 2002.</p>

        <ul>
          <li>Many codebases involve multiple different programming languages, especially web applications.</li>
          <li>Language popularity rankings have changed significantly, and will likely continue to change.</li>
          <li>Most "real-world" codebases are thousands or tens of thousands of lines of code.</li>
        </ul>

        <p>This means some core knowledge is <b>more</b> important than it used to be.</p>

        <ul>
          <li>Many programmers will need to learn multiple languages for a single job.</li>
          <li>Most programmers will need to learn <b>new</b> languages throughout their careers.</li>
          <li>Most programmers will usually work on a team of developers, each responsible for different parts of the same codebase.</li>
        </ul>

        <p>How can you do these things effectively?</p>

    <h3>Working with programming languages</h3>

      <p>To learn new languages and use them well, it's helpful to understand how languages work in general.</p>

      <p>That's one of the points of this course!</p>

      <p>If you take a high-level view, each language is a combination of individual <b>features</b>.</p>

      <h4>Learning languages</h4>

        <p>How can one programmer ever learn a dozen languages?</p>

        <p>Each language you learn teaches you some new concepts.</p>

        <p>When you learn your first language, all of the concepts are new, so it's a lot to learn.</p>

        <p>After you learn enough concepts, most languages only have a couple new concepts to teach you.</p>

        <p>This means if you want to get better at learning languages in general, you should try to learn a language that has many different features than the languages you already know.</p>

      <h4>Learning syntax</h4>

        <p>When you're learning a new language, learning new <i>syntax</i> is often the first barrier.</p>

        <p>Learning a new language often involves learning from <i>parsing errors</i>.</p>

        <p>Different languages are written different ways in source code, but they're all represented with ASTs.</p>

        <p>When you're learning new syntax, try to identify how it fits into the <b>nesting structure</b> of the code around it.</p>

      <h4>Learning semantics</h4>

        <p>After you know how to write a piece of code, you still have to learn how to use it.</p>

        <p>Learning the <i>semantics</i> of a feature often involves learning from <i>type errors</i> and <i>runtime errors</i>.</p>

        <p>Many new concepts you learn will come in the form of new kinds of <i>types</i>.</p>

        <p>Types are an organizing principle behind the features of most programming languages.</p>

        <p>In <i>execution</i>, concepts like <i>dynamic dispatch</i> define the behavior of language features.</p>

      <h4>Choosing a language</h4>

        <p>There is no "best" language in general, sadly. It would be nice if there was.</p>

        <p>Different languages are better for different tasks, and there is also a degree of personal preference.</p>

        <p>There are a couple major metrics that attempt to measure language <b>popularity</b>, including:</p>

        <ul>
          <li>the <a href="https://insights.stackoverflow.com/survey/2021#technology-most-popular-technologies">Stack Overflow Developer Survey</a>, which runs every year</li>
          <li>the <a href="https://redmonk.com/sogrady/2022/03/28/language-rankings-1-22/">RedMonk Programming Language Rankings</a>, based on GitHub and Stack Overflow activity</li>
          <li>the <a href="https://www.tiobe.com/tiobe-index/">TIOBE Index</a>, based on search engine results</li>
        </ul>

        <p>How can you choose the best language for some particular task?</p>

        <p>I can offer a couple bits of advice, at least:</p>

        <ul>
          <li>In general, practice working on a variety of programs in a variety of languages so that you can make your own comparisons from experience.</li>
          <li>Interact with the communities of various languages to get an idea of how other people are using each language.</li>
        </ul>

  </body>

</html>

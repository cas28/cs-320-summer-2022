<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <script src="../dist/bundle.js"></script>
    <link rel="stylesheet" href="../style/lecture.css">
    <title>Lecture 4: CS 320, Summer 2022</title>
  </head>

  <body>
    <h1>CS 320, Summer 2022</h1>
    <h2>Lecture 4: interpreter structure</h2>

    <h3>Introduction</h3>

      <p>In the assignments, we've seen some interpreter code.</p>

      <p>It was for a very tiny language, but it was technically an interpreter.</p>

      <p>What do interpreters for more realistic languages look like?</p>

      <p>This week, our focus is on the <i>structure</i> of a typical interpreter.</p>

      <p>We'll also see that compilers have almost the same structure.</p>

    <h3>Interpreter structure</h3>

      <p>Interpreter code can get pretty large and complex!</p>

      <p>A real-life interpreter benefits from careful code organization.</p>

      <p>We traditionally divide an interpreter into <i>phases</i>.</p>

      <p>The output of each phase is the input to the next phase.</p>

      <p>Here's a diagram of the whole process of running an interpreter:</p>

      <figure class="phases_figure">
        <table class="arrow_table">
          <tr><td class="arrow_label">bits</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
        <span class="phase">source input</span>
        <table class="arrow_table">
          <tr><td class="arrow_label">text</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
        <span class="phase">syntax analysis</span>
        <table class="arrow_table">
          <tr><td class="arrow_label">AST</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
        <span class="phase">static analysis</span>
        <table class="arrow_table">
          <tr><td class="arrow_label">AST</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
        <span class="phase">optimization</span>
        <table class="arrow_table">
          <tr><td class="arrow_label">AST</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
        <span class="phase">execution</span>
        <table class="arrow_table">
          <tr><td class="arrow_label">results</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
      </figure>

      <p>This is an approximate diagram: different interpreters are different.</p>

      <p>This is the traditional model that we use as a base to start from.</p>

      <p>Not all interpreters do static analysis or optimization. </p>

      <p>Some interpreters also have additional phases.</p>

      <p>Let's talk about each of these traditional phases.</p>

      <h4>Source input</h4>

        <p>We rarely think about this part: the OS handles it.</p>

        <p>Our goal is to go from source code to results.</p>

        <p>We think of our source code as <i>text</i>...</p>

        <p>But it's "really" just bits, or maybe electrical signals.</p>

        <p>Not every arbitrary sequence of bits is <i>text</i>!</p>

        <p>To process a text file, we need a way to <b>open it as a text file</b>.</p>

        <p>A file that isn't valid text is a <i>source input error</i>.</p>

        <p>Again, this isn't really part of our interpreter.</p>

        <p>It's just part of the process of running code in an interpreter.</p>

      <h4>Syntax analysis</h4>

        <p>Once we have text, we need to process it into an AST.</p>

        <p>This involves <i>parsing</i> algorithms, which we'll cover soon.</p>

        <p>Not all valid text files are valid <i>code</i>!</p>

        <p>If there is no valid AST for the input, it's a <i>parsing error</i>.</p>

        <p>In C++, <span class="code">1 $ % 2</span> is a parsing error.</p>

      <h4>Static analysis</h4>

        <p>Once we have an AST, we can check it for some kinds of errors.</p>

        <p>Not all interpreters have a static analysis phase.</p>

        <p>Another topic we'll see soon is <i>typechecking</i>.</p>

        <p>Typechecking is the most common kind of static analysis.</p>

        <p>If typechecking fails on a piece of code, it's a <i>type error</i>.</p>

        <p>In C++, <span class="code">1 + "xyz"</span> is a type error.</p>

        <p>The output of static analysis is still an AST, possibly with extra info.</p>

        <p>For example, it may insert extra type annotations into the AST.</p>

      <h4>Optimization</h4>

        <p>We can also transform the AST to make it run faster.</p>

        <p>The optimized AST must have <b>the same result</b> as the original AST.</p>

        <p>Optimizations often involve <i>rewriting</i> one code pattern into another.</p>

        <p>For example, it's (usually) safe to rewrite <span class="code">0 + x</span> to <span class="code">x</span>.</p>

        <p>The optimization phase should never throw an error.</p>

        <p>The output of optimization is a still an AST, possibly modified.</p>

      <h4>Execution</h4>

        <p>Finally, we traverse the AST to run the program.</p>

        <p>This phase is our core interpreter code.</p>

        <p>Any errors that occur during execution are <i>runtime errors</i>.</p>

        <p>In C++, <span class="code">1 / 0</span> is a runtime error.</p>

    <h3>Compiler structure</h3>

      <p>Here's that interpreter diagram again:</p>

      <figure class="phases_figure">
        <table class="arrow_table">
          <tr><td class="arrow_label">bits</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
        <span class="phase">source input</span>
        <table class="arrow_table">
          <tr><td class="arrow_label">text</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
        <span class="phase">syntax analysis</span>
        <table class="arrow_table">
          <tr><td class="arrow_label">AST</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
        <span class="phase">static analysis</span>
        <table class="arrow_table">
          <tr><td class="arrow_label">AST</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
        <span class="phase">optimization</span>
        <table class="arrow_table">
          <tr><td class="arrow_label">AST</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
        <span class="phase">execution</span>
        <table class="arrow_table">
          <tr><td class="arrow_label">results</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
      </figure>

      <p>A compiler only differs in the final phase:</p>

      <figure class="phases_figure">
        <table class="arrow_table">
          <tr><td class="arrow_label">bits</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
        <span class="phase">source input</span>
        <table class="arrow_table">
          <tr><td class="arrow_label">text</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
        <span class="phase">syntax analysis</span>
        <table class="arrow_table">
          <tr><td class="arrow_label">AST</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
        <span class="phase">static analysis</span>
        <table class="arrow_table">
          <tr><td class="arrow_label">AST</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
        <span class="phase">optimization</span>
        <table class="arrow_table">
          <tr><td class="arrow_label">AST</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
        <span class="phase">code generation</span>
        <table class="arrow_table">
          <tr><td class="arrow_label">executable</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
      </figure>

      <p>The <i>code generation</i> phase is the core translation code.</p>

      <p>We won't have time to cover code generation in this course, sadly.</p>

      <p>Keep an eye out for compiler courses if you're interested!</p>

    <h3>Implementation</h3>

      <p>In general, each phase represents a function.</p>

      <p>The arrows represent the input and output types of each function.</p>

      <p>The "results" type in the execution phase may be <span class="code">void</span>.</p>

      <p>For our tiny calculator so far, the "results" type is <span class="code">number</span>.</p>

    <h3>Looking forward</h3>

      <p>For the next few weeks, we will be exploring each phase in detail.</p>

      <p>Well, most of the phases: we'll skip <i>source input</i>.</p>

      <p>(That's more for an OS course than a PL course.)</p>

      <p>Our next major topic is <i>syntax analysis</i>.</p>

      <p>Our little calculator language will start to grow pretty quickly!</p>

  </body>
</html>

<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <script src="../dist/bundle.js"></script>
    <link rel="stylesheet" href="../style/lecture.css">
    <title>Lecture 9: CS 320, Summer 2022</title>
  </head>

  <body>

    <h1>CS 320, Summer 2022</h1>
    <h2>Lecture 9: programming language paradigms</h2>

    <h3>Introduction</h3>

      <p>At this point, we've covered the <i>syntax</i> and <i>semantics</i> of <i>imperative programming languages</i>.</p>

      <p>Let's review what that means for a moment:</p>

      <ul>
        <li><i>syntax</i>:
          <ul>
            <li>the "grammar" or "structure" of a program</li>
            <li>represented in code with <i>text</i> and <i>ASTs</i></li>
            <li>processed in code by <i>regexes</i> and <i>CFGs</i></li>
          </ul>
        </li>
        <li><i>semantics</i>:
          <ul>
            <li>the "meaning" or "behavior" of a program</li>
            <li><i>static semantics</i>:
              <ul>
                <li>what we can know <b>without</b> <i>executing</i> the program</li>
                <li><i>static analysis</i>: predicting errors</li>
                <li><i>typechecking</i>: most common form of static analysis</li>
              </ul>
            </li>
            <li><i>dynamic semantics</i>:
              <ul>
                <li>what we can know <b>by</b> <i>executing</i> the program</li>
                <li><i>execution</i>: running code, possibly hitting errors</li>
              </ul>
            </li>
          </ul>
          <li><i>imperative programming langauge</i>:
            <ul>
              <li><i>expressions</i>: code that <i>evaluates</i> to a <i>value</i></li>
              <li><i>statements</i>: code that has no <i>value</i>, only <i>side effects</i></li>
              <li><i>sequential execution</i>: "do this thing <b>and then</b> do this other thing"</li>
            </ul>
          </li>
        </li>
      </ul>

      <p>We've explored imperative programming from the inside out, pretty thoroughly.</p>

      <p>Most of our modern languages have imperative features, but most are not <i>purely</i> imperative.</p>

      <p>What other kinds of language features are there?</p>

      <p>In this lecture, we'll introduce the idea of a <i>language paradigm</i>, and briefly survey the most common modern and historical paradigms.</p>

    <h3>Categorizing natural languages</h3>

      <p>Natural languages don't each exist in isolation from each other, right?</p>

      <p>Many natural languages share common "features" with other natural languages.</p>

      <p>The field of linguistics studies these commonalities between natural languages.</p>

      <p>We can define broad categories of languages that share common features.</p>

      <h4>Language families</h4>

        <p>Historically, natural languages <b>inherit from</b> other natural languages.</p>

        <p>For example, both Spanish and French are <i>descendents</i> of Latin.</p>

        <p>This is a <i>family relationship</i>, with a rough "parent"/"child"/"sibling" metaphor.</p>

        <p>We think of a language family as a <i>tree</i> of languages.</p>

      <h4>Language paradigms</h4>

        <p>Sometimes natural languages independently develop similar features.</p>

        <p>For example, both Turkish and Japanese are considered "agglutinative" languages.</p>

        <p>They <b>don't</b> share a common ancestor.</p>

        <p>(Or if they do, they both diverged from it several thousand years ago, before either language became agglutinative.)</p>

        <p>Both languages just happen to have the feature of "agglutination".</p>

        <p>In PL terms, we might say both languages are examples of "the agglutinative <i>language paradigm</i>".</p>

        <p>(I don't think linguists use the word "paradigm" in this way, to be clear. This is just a metaphor to introduce how PL terminology works.)</p>

        <p>We think of a language <i>paradigm</i> as a <i>set</i> of features, and the set of languages that share those features.</p>

        <p>A language can be <i>multi-paradigm</i>, and most real-world languages are.</p>

        <p>Turkish and Japanese both also have many <b>other</b> features, independent from agglutination.</p>

        <p>In general, a <i>paradigm</i> is really just a set of features that's common to multiple languages.</p>

        <p>More specifically, we usually use the word <i>paradigm</i> for a set of features that strongly influences how we <b>use</b> a language.</p>

    <h3>Categorizing programming languages</h3>

      <p>In the same way, programming languages don't exist in isolation from each other.</p>

      <p>Programming languages also share features with other programming languages.</p>

      <p>The field of PL studies these commonalities between programming languages.</p>

      <h4>Language families</h4>

        <p>In PL, C++ is usually considered a <i>descendent</i> of C.</p>

        <p>It borrows almost all of the syntax and semantics, and adds more on top.</p>

        <p>In general, "language families" in PL are pretty minimal: it's a very young field.</p>

        <p>They're also inherently vague: C# could be considered either a <i>descendant</i> or a <i>sibling</i> of Java.</p>

      <h4>Language paradigms</h4>

        <p>In PL, we tend to categorize languages primarily in <i>paradigms</i>.</p>

        <p>Our toy language from assignment 5 implements (at least) two paradigms:</p>

        <ul>
          <li><i>imperative programming</i>, where we have <i>sequential execution</i> of <i>statements</i></li>
          <li><i>structured programming</i>, where we have conditionals (<span class="code">if</span>) and loops (<span class="code">while</span>)</li>
        </ul>

        <p>Where do these paradigms come from?</p>

        <p>How do they affect the way we write programs?</p>

        <p>What other features could a language have <b>instead</b> of these features?</p>

        <p>What other features could a language have <b>in addition to</b> these features?</p>

      <h4>Control flow</h4>

        <p>One very common concept that we use to analyze paradigms is <i>control flow</i>.</p>

        <p>In general, the <i>control flow</i> of a program is the order that it does things in.</p>

        <p>At each step in the program's execution, how do we know what happens next?</p>

        <p>Each paradigm we survey will have answers to this question.</p>

      <h4>Multi-paradigm langauges</h4>

        <p>Most modern programming languages are <i>multi-paradigm</i>.</p>

        <p>This really just means they include features from more than one paradigm.</p>

        <p>There is a tradeoff: the language becomes more <i>expressive</i>, but also more complex.</p>

        <p>Language complexity is generally bad for program <i>predictability</i>.</p>
        
        <p>Different multi-paradigm languages manage this tradeoff in very different ways.</p>

        <p>We'll explore this concept when we turn our attention to TypeScript itself in upcoming lectures.</p>

    <h3>Historical paradigms</h3>

      <p>The history of programming languages can be seen as a nonlinear progression of paradigms.</p>

      <p>Some paradigms are mostly of historical interest now, because other paradigms succeeded them.</p>

      <p>Specifically, let's discuss the <i>imperative</i>, <i>structured</i>, and <i>procedural</i> paradigms.</p>

      <h4>Imperative programming</h4>

        <p>In the history of PL, <i>imperative</i> languages were the first to run on actual computing hardware.</p>

        <p>(Ignoring things like Charles Babbage's Difference Engine, which is more like a "calculator" than a "computer" in modern terminology.)</p>

        <p>The machine languages of modern processors are imperative languages.</p>

        <p>In a sense, a modern processor is a fundamentally imperative machine.</p>

        <p>Not many other languages are <i>purely</i> imperative: most have other features too.</p>

        <p>Some very early dialects of BASIC are purely imperative languages.</p>

        <p>Imperative languages have the feature of <i>sequential execution</i>.</p>

        <p>In sequential execution, <i>statements</i> are ordered, and control flow proceeds from one statement to the next in a linear order.</p>

        <p>Execution starts with the first statement.</p>

        <p>Purely imperative languages usually have <i>jump statements</i> (like <span class="code">goto</span>).</p>

        <p>A jump statement changes the control flow to jump to a specific statement, which usually may be anywhere in the whole program.</p>

      <h4>Structured programming</h4>

        <p><i>Structured programming</i> is variation of <i>imperative programming</i>.</p>

        <p>In practice, jump statements are a maintenance nightmare.</p>

        <p>Any line of code can jump to any other line of code at any time!</p>

        <p>It can be very hard to <i>predict</i> the behavior of code with unrestricted jumps.</p>

        <p>Around the mid-1950s, programmers started inventing features to avoid manually writing jumps.</p>

        <p>In 1968, Edsger Dijkstra published an open letter called "Go To Statement Considered Harmful", and coined the term <i>structured programming</i>.</p>

        <p>Today, structured programming is associated with three main <i>control structures</i>:</p>

        <ul>
          <li><i>block statements</i>, with <i>sequential</i> control flow</li>
          <li><i>conditional statements</i>, with <i>branching</i> control flow</li>
          <li><i>loop statements</i>, with <i>iterative</i> control flow</li>
        </ul>

        <p>In 1966, Böhm and Jacopini showed that these three statement forms can replace <b>any</b> use of <span class="code">goto</span>.</p>

        <p>This is often referred to as the <i>structured programming theorem</i>.</p>

        <p>(They actually proved something more general, but this is a consequence of it.)</p>

      <h4>Procedural programming</h4>

        <p><i>Procedural</i> programming is an extension of <i>structural</i> programming.</p>

        <p>Most early languages that we still talk about are considered <i>procedural</i> languages:</p>

        <ul>
          <li>Fortran (John Backus, 1957)</li>
          <li>COBOL (Howard Bromberg + Howard Discount + Vernon Reeves + Jean E. Sammet + William Selden + Gertrude Tierney, 1959)</li>
          <li>BASIC (John Kemeny + Thomas Kurtz, 1964)</li>
          <li>Pascal (Niklaus Wirth, 1970)</li>
          <li>C (Dennis Ritchie, 1972)</li>
          <li>Ada (Jean Ichbiah, 1983)</li>
          <li>...</li>
        </ul>

        <p>In modern terminology, <i>function calls</i> are the fundamental feature of procedural programming.</p>

        <p>Historically, procedural languages often distinguished between:</p>

        <ul>
          <li><i>subroutines</i>, which are just jumps with a <span class="code">return</span> command</li>
          <li><i>procedures</i>, which are subroutines with arguments and local scope</li>
          <li><i>functions</i>, which are procedures that return a result</li>
        </ul>

        <p>In most modern languages, we call all of these things "functions":</p>

        <ul>
          <li><i>subroutines</i> are like zero-argument <span class="code">void</span> functions</li>
          <li><i>procedures</i> are like <span class="code">void</span> functions with arguments</li>
        </ul>

        <p>The <i>control flow</i> of a function call is subtle!</p>

        <p>To review:</p>

        <ul>
          <li>When we call a function, we create a <i>frame</i> (or <i>scope</i>) for it.</li>
          <li>A stack frame contains a function's <i>arguments</i> and <i>local variables</i>.</li>
          <li>Each stack frame also contains a pointer to <b>where it was called from</b>.</li>
          <li>Each time we call a function, we push a frame for it onto our <i>call stack</i> (or <i>activation stack</i>).</li>
          <li>After pushing the frame for a function, <i>control flow</i> transfers to its <i>body</i>.</li>
          <li>When a function returns, control flow transfers to <b>where it was called from</b>.</li>
          <li>If the function returns a value, the value goes to <b>where it was called from</b>.</li>
        </ul>

        <p>One consequence of the procedural paradigm is that we can write <i>recursive</i> programs.</p>

        <p>Remember: recursion is possible on an imperative machine because of the <i>call stack</i>.</p>

    <h3>Modern paradigms</h3>

      <p>Today, there are many relevant paradigms, and the word "paradigm" has gotten vaguer than before.</p>

      <p>Most modern paradigms are compatible with each other, at least in principle.</p>

      <p>Here are a few modern sets of features commonly identified as <i>paradigms</i>:</p>

      <h4>Object-oriented programming (OOP)</h4>
        <ul>
          <li>defined by <i>dynamic dispatch</i>: in <span class="code">x.foo()</span>, the jump target for <span class="code">foo</span> is determined at <i>runtime</i></li>
          <li>common other features: <i>inheritance</i>, <i>classes</i>, <i>interfaces</i></li>
          <li>usually considered an extension of <i>procedural</i> programming</li>
        </ul>

      <h4>Functional programming (FP)</h4>
        <ul>
          <li>defined by first-class functions: <i>function definitions</i> are <i>values</i></li>
          <li>common other features: pattern matching, algebraic data types, typeclasses (or traits)</li>
          <li>often contrasted with <i>imperative programming</i>, but not incompatible with it</li>
        </ul>

      <h4>Parallel programming</h4>
        <ul>
          <li>defined by multiple physical computing cores working together simultaneously</li>
          <li>common features: synchronization primitives, parallel control structures</li>
          <li>theories
        </ul>

      <h4>Logic programming</h4>
        <ul>
          <li>defined by queries that compute the result of a predicate logic question</li>
          <li>common features: free variables, pattern matching</li>
        </ul>

      <h4>Metaprogramming</h4>
        <ul>
          <li>defined by programs that generate code in their own <i>source</i> or <i>target language</i></li>
          <li>common features: macros, templates, fresh name generation</li>
        </ul>

      <p>In upcoming lectures, we will survey <i>OOP</i> and <i>FP</i> in a little more depth.</p>

  </body>

</html>

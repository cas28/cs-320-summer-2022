<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <script src="../dist/bundle.js"></script>
    <link rel="stylesheet" href="../style/lecture.css">
    <title>Lecture 11: CS 320, Summer 2022</title>
  </head>

  <body>

    <h1>CS 320, Summer 2022</h1>
    <h2>Lecture 11: Survey of functional programming</h2>

    <h3>Introduction</h3>

      <p>Last lecture, we discussed the paradigm of <i>object-oriented programming (OOP)</i>.</p>

      <p>OOP is often considered a direct descendant of the <i>procedural</i> paradigm.</p>

      <p>This gives it a more-or-less direct lineage going back to the 1920s:</p>

      <ol>
        <li>Turing machines (original theory of imperative hardware)</li>
        <li>Harvard and von Neumann architectures (original theories of "modern processors")</li>
        <li>Imperative programming</li>
        <li>Structured programming</li>
        <li>Procedural programming</li>
        <li>Object-oriented programming</li>
      </ol>

      <p>This is just one particular line of "descent":</p>

      <ul>
        <li>The concurrent paradigm also derives from the procedural paradigm.</li>
        <li>The array-oriented paradigm also derives from the imperative paradigm.</li>
        <li>etc.</li>
      </ul>

      <p>There are also descendants of OOP, like aspect-oriented programming.</p>

      <p>All of these paradigms inherit from the original Turing machine design.</p>

      <p>How does this affect the design of languages in these paradigms?</p>

      <p>Are there any paradigms that don't inherit from Turing machines?</p>

      <p>In this lecture, these questions will begin our discussion of the <i>functional programming</i> paradigm.</p>

      <p>As usual, we will not have time to go very deep at all!</p>

      <p>CS 457 offers a much deeper look into functional programming.</p>

    <h3>Theories of computation</h3>

      <p>This will be a <b>very</b> brief overview of the history of the theory of computation.</p>

      <p>You can study this subject in much more depth in a course like CS 311!</p>

      <h4>Prehistory</h4>

        <p>In the early 20th century, the word "computer" was the title of a job held by human beings.</p>

        <p>This is not a joke! English has changed quite a bit in the past century.</p>

        <p>If you've seen the movie Hidden Figures, many of the people depicted in that movie had the official job title of "computer" in real life.</p>

        <p>In those days, the task of "engineering" was split between two roles:</p>

        <ul>
          <li>An "engineer" was tasked with taking some product idea and figuring out how to implement it with actual physical components.</li>
          <li>A "computer" was tasked with performing calculations when an "engineer" needed the result of a calculation in their design process.</li>
        </ul>

        <p>Both tasks were hard work: the "computers" were often solving differential equations for physical modeling problems.</p>

        <p>The "computers" also had to <b>figure out how</b> to solve each problem given to them.</p>

        <p>Many people who were "computers" became early pioneers in the emerging field of "computer science".</p>

        <p>Despite all that, there is a lot of historical discrimination where "computers" were seen as lesser workers.</p>

        <p>This was the economic setting for the development of modern "electronic computers".</p>

        <p>People started wondering: can we get a machine to solve these math problems for us?</p>

      <h4>Early theories</h4>

        <p>The next question was: well, what <b>exactly</b> do we mean by "math problems"?</p>

        <p>What is reasonable or unreasonable to ask of a "computer"?</p>

        <p>Mathematicians started proposing various definitions of "computability".</p>

        <p>Most famously:</p>

        <ul>
          <li>Kurt G&#246;del defined the theory of <i>general recursive functions</i>.</li>
          <li>Alonzo Church defined the theory of <i>lambda-defined functions</i>.</li>
          <li>Alan Turing defined the theory of <i>Turing machines</i>.</li>
        </ul>

        <p>In a modern sense, we can roughly consider each of these a different <i>programming language</i>.</p>

        <p>These people all knew each other, and Church was Turing's thesis advisor.</p>

        <p>In the early 1940s, Church, Turing, and Stephen Kleene published a series of papers now known collectively as the <i>Church-Turing thesis</i>.</p>

        <p>Very roughly, the Church-Turing thesis proves that anything possible in one of these languages is possible in all three:</p>

        <ul>
          <li>A <i>general recursive function</i> can do anything that a <i>lambda-defined function</i> can do.</li>
          <li>A <i>lambda-defined function</i> can do anything that a <i>Turing machine</i> can do.</li>
          <li>A <i>Turing machine</i> can do anything that a <i>general recursive function</i> can do.</li>
        </ul>

        <p>In modern terms, we might say each of these three languages is equivalently "powerful".</p>

        <p>(This doesn't mean they're equally <i>efficient</i> in all cases!)</p>

        <p>Turing and Kleene both hypothesized that it is impossible to create a more "powerful" language.</p>

        <p>This is generally accepted to be true, because it's been a century and it hasn't been proven false.</p>

        <p>Technically it hasn't been proven true either, because it's hard to even define the hypothesis mathematically.</p>

      <h4>Early hardware</h4>

        <p>If each theory is equivalent, which one should we use to build our computers?</p>

        <p>The Turing machine turned out to be the easiest to actually build in reality.</p>

        <p>(This isn't a coincidence - Alan Turing designed it with that purpose in mind.)</p>

        <p>The Turing machine is fundamentally <i>imperative</i> hardware.</p>

        <p>This is where we get our modern idea of machine code <i>instructions</i>.</p>

        <p>Machine code only has <i>statements</i>, not <i>expressions</i>.</p>

      <h4>Statement-based execution</h4>

        <p>Consider the expression <span class="code">(w * x) + (y * z)</span>.</p>

        <p>This is a <i>nested expression</i>, with two immediate subexpressions.</p>

        <p>In pseudo-code with only statements, the program may look something like this:</p>

        <pre class="code">
let a: number;
multiply_into(w, x, a);
let b: number;
multiply_into(y, z, b);
let c: number;
add_into(a, b, c);
return c;</pre>

        <p>To translate a nested expression into a statement-based language, we have to make a lot of choices about the <i>ordering</i> of statements.</p>

        <p>For example, this pseudo-code program gives the same result:</p>

        <pre class="code">
let a: number;
let b: number;
let c: number;
multiply_into(w, x, a);
multiply_into(y, z, b);
add_into(a, b, c);
return c;</pre>

        <p>In very specific circumstances, the programmer <b>might</b> care about the fact that the two programs have a different ordering of instructions, even though the result is the same.</p>

        <p>But in most cases they don't, right?</p>

        <p>In most cases you just want to write <span class="code">(w * x) + (y * z)</span> and let the compiler figure it out.</p>

    <h3>Declarative programming</h3>

      <p>There is a big idea here: <span class="code">(w * x) + (y * z)</span> is more <i>declarative</i> than the statement-based version.</p>

      <h4>Terminology</h4>

        <p>The term <i>declarative</i> is related to the concept of <i>high-level</i> code.</p>

        <p>In general, declarative code specifies <b>what</b> to do, without specifying <b>exactly how</b> to do it.</p>

        <p>More specifically, declarative code leaves out details that are relevant to the Turing machine but not to the programmer.</p>

        <p>In this sense, nested expressions are fundamentally declarative.</p>

        <p>They are far from the only declarative language feature, though.</p>

        <p>The goal of a declarative program is to express a <b>problem</b> in precise terms, so that the computer can <b>figure out how to solve</b> the problem.</p>

      <h4>Prehistory</h4>

        <p>Remember the human "computers"?</p>

        <p>Remember how they solved differential equations on their own?</p>

        <p>No modern processor architectures have a <b>single instruction</b> for that.</p>

        <p>The experience of working with a human "computer" was way more <i>declarative</i> than the experience of working with a modern electronic "computer".</p>

        <p>This was recognized in the early days of PL, and motivated many language designs.</p>

        <p>The goal was for the computer to "understand" the human better than it can with hand-written machine code.</p>

      <h4>Declarative programming and AI</h4>

        <p>In a modern context, this starts to sound a lot like AI.</p>

        <p>The early history of declarative programming is also the early history of AI.</p>

        <p>The two fields diverged later, partly due to the invention of machine learning.</p>

        <p>There is a fundamental difference: a declarative language is expected to be <i>deterministic</i>.</p>

        <p>It's not good if our language has a chance of <b>randomly</b> doing the wrong thing!</p>

        <p>Machine learning is fundamentally based on <i>probability</i>.</p>

        <p>Because of this, machine learning is (at present) usually incapable of giving the kinds of hard guarantees we expect in a <i>programming language</i>.</p>

        <p>Today, AI is mostly based on machine learning.</p>

        <p>This makes modern AI and modern declarative programming only loosely related.</p>

        <p>If we zoom out very far, their goals have some rough overlap, but their approaches are very different.</p>

      <h4>Declarative paradigms</h4>

        <p>There is not really a single paradigm of "declarative programming".</p>

        <p>"Declarative programming" is more like an ideal that motivates some paradigms.</p>

        <p>These include:</p>

        <ul>
          <li>Functional programming (FP)</li>
          <li>Logic programming</li>
          <li>Constraint-based programming</li>
        </ul>

        <p>To varying degrees, languages in these paradigms <b>hide</b> the underlying imperative behavior of hardware.</p>

        <p>Instead, they present the programmer with a fundamentally different notion of computation.</p>

        <p>A <i>compiler</i> or <i>interpreter</i> bridges the gap between the different ideas of computation.</p>

    <h3>Functional programming</h3>

      <p>Okay, remember those three theories of computability?</p>

      <p>The <i>Turing machine</i> turned out to be the origin of <i>imperative hardware</i>.</p>

      <p>From there came <i>imperative languages</i>, and all the paradigms they led to.</p>

      <p><i>Functional programming</i> comes from the other two theories.</p>

      <h4>Lambda calculus</h4>

        <p>Church's theory of <i>lambda-defined functions</i> is now more commonly called <i>lambda calculus</i>.</p>

        <p>This has nothing to do with derivative or integral calculus, to be clear!</p>

        <p>The word <i>calculus</i> just means "a system for doing calculations".</p>

        <p>Lambda calculus is the historical origin of FP.</p>

        <p>From lambda calculus, we get the language feature of <i>first-class functions</i>.</p>

        <p>This is the idea we've discussed in TypeScript: <b>functions are values</b>.</p>

        <p>Nearly every modern language supports a form of first-class functions.</p>

        <p>Also, nearly every modern language supports <i>anonymous functions</i>:</p>

        <ul>
          <li>TypeScript: <span class="code">function (x: number) { return 2 * x; }</span></li>
          <li>TypeScript (alternate): <span class="code">(x: number) =&gt; 2 * x</span></li>
          <li>C++: <span class="code">[](int x) { return 2 * x; }</span></li>
          <li>Java: <span class="code">(int x) -&gt; 2 * x</span></li>
          <li>Python: <span class="code">lambda x: 2 * x</span></li>
          <li>...</li>
        </ul>

        <p>Python reveals the history of this feature, from lambda calculus written on paper:</p>

        <ul>
          <li>Lambda calculus: <span class="code">&lambda;x. 2 * x</span></li>
        </ul>

      <h4>General recursive functions</h4>

        <p>From G&#246;del's theory of <i>general recursive functions</i>, we get the feature of <i>general recursion</i>.</p>

        <p>Lambda calculus is theoretically capable of recursion, but it's inconvenient.</p>

        <p>(Remember, each of the three theories is equivalently powerful.)</p>

        <p>The feature of <i>general recursion</i> is our modern convenient form of recursion.</p>

        <p>Specifically, <b>a function can call itself freely by referencing its own name</b>.

        <p>This feature is relevant to both the <i>procedural</i> and <i>functional</i> paradigms.</p>

      <h4>Functional language families</h4>

        <p>There are two major historical families in the functional paradigm:</p>

        <ul>
          <li>LISP, the origin of <i>dynamically-typed</i> FP</li>
          <li>ML, the origin of <i>statically-typed</i> FP</li>
        </ul>

        <p>ML does not stand for "machine learning" here! It stands for "Meta Language".</p>

        <p>The language predates the field of machine learning (and also Facebook's name change).</p>

    <h3>LISP</h3>

      <p>LISP is generally considered to be the first functional programming language.</p>

      <h4>History</h4>

        <p>In 1960, John McCarthy published "Recursive Functions of Symbolic Expressions and Their Computation by Machine, Part I".</p>

        <p>The ambitions of his project were pretty grand - to quote the paper:</p>

        <blockquote>
          The system was designed to facilitate experiments with a proposed system called the Advice Taker, whereby a machine could be instructed to handle declarative as well as imperative sentences and could exhibit “common sense” in carrying out its instructions.
        </blockquote>

        <p>The AI aspects of this project fell apart early on, in what's now often called the "AI winter".</p>

        <p>The enduring influence of this project is in the LISP language that McCarthy developed for it.</p>

        <p>Another motivation behind LISP shows up in this quote:</p>

        <blockquote>
          We believe this formalism has advantages both as a programming language and as a vehicle for developing a theory of computation.
        </blockquote>

        <p>In other words, LISP is designed to be a <b>tool of thought</b> as much as a programming language.</p>

      <h4>Major features</h4>

        <p>LISP is heavily influenced by both <i>lambda calculus</i> and <i>general recursion</i>.</p>

        <p>LISP supports <i>first-class functions</i> in the same form we use them today.</p>

        <p>Here's an example of an <i>anonymous function</i> from McCarthy's 1960 paper:</p>

        <blockquote>
          λ((x, y), y<sup>2</sup> + x) is a function of two variables, and λ((x, y), y<sup>2</sup> + x)(3, 4) = 19.
        </blockquote>

        <p>LISP is also the first major <i>dynamically-typed</i> language.</p>

        <p>One of LISP's biggest contributions to PL is outside the paradigm of FP.</p>

        <p>LISP shares the feature of <i>macros</i> with C, but LISP's macros are significantly more powerful.</p>

        <p>Macros are a core feature of the <i>metaprogramming</i> paradigm, which also traces its origins back to LISP.</p>

      <h4>Homoiconic syntax</h4>

        <p>Syntactically, LISP is characterized by a list-based syntax.</p>

        <p>This involves a lot of parentheses, but the parentheses aren't the point!</p>

        <p>The point is that the text of a program <b>directly</b> corresponds to its <i>abstract syntax tree</i>.</p>

        <p>This is a reflection of LISP's focus on ASTs as a data structure.</p>

        <p>Any LISP program can manipulate its <b>own</b> AST, which enables LISP's powerful macros.</p>

        <p>This is a distinguishing feature of LISP, known as <i>homoiconic syntax</i>.</p>

      <h4>Descendants</h4>

        <p>LISP has a few direct descendants in the modern world:</p>

        <ul>
          <li>Scheme (1975): hygienic macros</li>
          <li>Common Lisp (1984): OOP features and a general-purpose standard library</li>
          <li>Racket (1995): extended macro system, designed for building new languages</li>
          <li>Clojure (2007): interoperability with Java and other JVM languages</li>
        </ul>

        <p>Many languages that are not direct descendants also take take some influence from LISP:</p>

        <ul>
          <li>Smalltalk, Python, JavaScript, Lua: dynamic types</li>
          <li>Rust, Haskell: hygienic macros</li>
          <li>Nearly all modern languages: anonymous functions, first-class functions</li>
        </ul>

    <h3>ML</h3>

      <p>ML is generally considered to be the first <i>statically-typed</i> functional programming language.</p>

      <h4>History</h4>

        <p>In 1966, Peter Landin published an article titled "The Next 700 Programming Languages".</p>

        <p>In it, he sketches out a pseudo-language called ISWIM, for "I say what I mean".</p>

        <p>Here's a quote, with some additional formatting applied:</p>

        <blockquote>
          The commonplace expressions of arithmetic and algebra have a certain simplicity that most communications to computers lack. In particular,
          <ol type="a">
            <li>each expression has a nesting subexpression structure,</li>
            <li>each subexpression denotes something (usually a number, truth value or numerical function),</li>
            <li>the thing an expression denotes, i.e., its "value", depends only on the values of its sub- expressions, not on other properties of them.</li>
          </ol>
        </blockquote>

        <p>Landin is describing a language that has <b>only <i>expressions</i>, no <i>statements</i></b>.</p>

        <p>In modern terms, we sometimes refer to this as <i>expression-based programming</i>.</p>

        <p>Landin also coins the term <i>purely-functional</i> in this quote:</p>

        <blockquote>
          It includes a nonprocedural (purely functional) subsystem fhat aims to expand the class of users' needs that can be met by a single print-instruction, without sacrificing the important properties that make conventional right-hand-side expressions easy to construct and understand.
        </blockquote>

        <p>ISWIM is just a pseudo-language; nobody ever directly implements it.</p>

        <p>In 1973, Robin Milner leads a team that invents the ML language.</p>

        <p>ML is directly inspired by ISWIM, but has an actual compiler.</p>

        <p>The same work also leads to the Hindley-Milner algorithm, which is one of the first major results in the study of <i>type inference</i>.</p>

      <h4>Major features</h4>

        <p>ML adds <i>static typechecking</i> to the ISWIM design.</p>

        <p>As a consequence, ML also introduces the modern concept of a <i>function type</i>.</p>

        <p>Historically, ML is significant as the first major language with a <b>full</b> <i>correctness specification</i> for how its compiler should behave.</p>

        <p>ML is also notable for its module system, which implements a form of module polymorphism.</p>

        <p>Later dialects of ML popularized algebraic data types, which enable the feature of pattern matching.</p>

      <h4>Descendants</h4>

        <p>ML has a few descendants in the modern world:</p>

        <ul>
          <li>SML (1983): the modern dialect of the original ML project</li>
          <li>OCaml (1996): OOP features</li>
          <li>F# (2005): interoperability with C# and other CLR languages</li>
        </ul>

        <p>Haskell (1990) is also a close relative of ML, but not quite a direct descendant.</p>

        <p>Many languages that are not direct descendants also take some influence from ML:</p>

        <ul>
          <li>Nearly all modern statically-typed languages: function types, type inference</li>
          <li>Rust: expression-based programming</li>
          <li>Rust, Swift, Haskell: algebraic data types</li>
          <li>Scala, C# (version 7), Python (version 3.10): pattern matching</li>
        </ul>

    <h3>Modern functional languages</h3>

      <p>Today, nearly every language supports <i>first-class functions</i>.</p>

      <p>In this sense, nearly every language is a "functional language".</p>

      <p>Usually, the languages we call "functional" today are focused on <i>purely</i> functional programming.</p>

      <p>These are probably the most notable examples:</p>

      <ul>
        <li>Haskell (1990): fully pure, cutting-edge type system</li>
        <li>Scala (2004): combination of Java-style OOP and ML-style FP</li>
        <li>Rust (2010): ML carefully disguised as a cousin of C++</li>
      </ul>

    <h3>Looking forward</h3>

      <p>There's so much more to explore about FP, but that's probably all we can fit in!</p>

      <p>In our final lectures, we'll survey the current language landscape, and review how the PL concepts we've discussed come up in everyday programming.</p>

  </body>

</html>

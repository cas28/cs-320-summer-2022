<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <script src="../dist/bundle.js"></script>
    <link rel="stylesheet" href="../style/lecture.css">
    <title>Lecture 3: CS 320, Summer 2022</title>
  </head>

  <body>
    <h1>CS 320, Summer 2022</h1>
    <h2>Lecture 3: data structures</h2>

    <h3>Introduction</h3>

      <p>From previous courses, you should be familiar with <i>singly-linked lists</i>.</p>

      <p>Singly-linked lists are (roughly) the simplest <i>recursive data structure</i>.</p>

      <p>Much of PL is founded on another recursive data structure: <i>trees</i>.</p>

      <p>Each language we work with has an associated tree data structure.</p>

      <p>The ultimate goal of today's lecture is to introduce these <i>abstract syntax trees</i>.</p>

      <p>To get there, we'll do some very brief review of data structures in general.</p>

    <h3>Data structures</h3>

      <h4>Mutability</h4>

        <p>You're probably used to <i>mutable</i> data structures, whose values can be modified after creation.</p>

        <p>These do come up in PL work, but less often than <i>immutable</i> data structures.</p>

        <p>Most of our algorithms are <i>non-destructive</i>: we may want to reuse the input later.</p>

        <p>Immutable data structures guarantee we don't modify our input data.</p>

        <p>This is less slow than you'd imagine: we can always do "shallow" copies.</p>

        <p>Most PLs support immutable data with keywords like <span class="code">const</span>, <span class="code">final</span>, or <span class="code">readonly</span>.

      <h4>Type systems</h4>

        <p>The <i>type system</i> of a language is the part that relates to <i>typechecking</i>.</p>

        <p>Almost all languages do some kind of typechecking, though details vary widely.</p>

        <p>Later in this course, we'll study the theory of typechecking a little bit.</p>

        <p>For now, we'll focus on the type system features that we'll be using for our trees.</p>

    <h3>Product types</h3>

      <p>Most PLs support bundling different types into one combined type.</p>

      <p>There are different ways to do this, but all have at least two common features:
        <ul>
          <li>We can <b>construct</b> a value by bundling together "smaller" values.</li>
          <li>We can <b>access</b> one or more of the "smaller" values within a bundled value.</li>
        </ul>
      </p>

      <p>In PL theory, this general concept is known as a <i>product type</i>.</p>

      <h4>Purpose</h4>

        <p>Product types are the "and" in our data types.</p>

        <p>A product type of <span class="code">A</span> and <span class="code">B</span> contains an <span class="code">A</span> value <b>and</b> a <span class="code">B</span> value.</p>

        <p>Specifically, a product type has a <b>fixed</b> number of "smaller" values.</p>

        <p>This includes most language features called "classes", "structs", "records", and "tuples".</p>

        <p>This usually doesn't include arrays and lists: they usually have a <b>varying</b> number of "smaller" values.</p>

      <h4>Terminology</h4>

        <p>Why "product" type? It's sort of very vaguely like multiplication.</p>

        <p>We'll use |<span class="code">A</span>| to mean "the total number of different possible A values".</p>

        <p>For example, |<span class="code">bool</span>| is 2 (in most languages).

        <p>Some types have an infinite number of different values, like <span class="code">string</span>.</p>

        <p>If <span class="code">X</span> is a product type of <span class="code">A</span> and <span class="code">B</span>, then |<span class="code">X</span>| is |<span class="code">A</span>|*|<span class="code">B</span>|.</p>

        <p>For example, there are 2*2=4 possible pairs of <span class="code">bool</span>s:
          <ul>
            <li><span class="code">true</span>, <span class="code">true</span>
            <li><span class="code">false</span>, <span class="code">true</span>
            <li><span class="code">true</span>, <span class="code">false</span>
            <li><span class="code">false</span>, <span class="code">false</span>
          </ul>
        </p>

        <p>It's very hard to come up with good names for abstract concepts.</p>

        <p>The name "product" comes from category theory, which is mostly a historical detail.</p>

        <p>A better name might be "AND type", but that would get confusing to say.</p>

      <h4>In TypeScript</h4>

        <p>In TypeScript, our main way to declare product types is with <i>object types</i>.</p>

        <p>These are very similar to <i>classes</i>, but we'll see later in the course that they're slightly different.</p>

        <p>This object type "bundles" a <span class="code">number</span> and a <span class="code">string</span>:
          <pre class="code">
type Example1 = {
  x: number;
  y: string;
}
          </pre>
        </p>

        <p>We can construct an <span class="code">Example1</span> value by initializing each of its fields within a pair of curly braces:</p>
          <pre class="code">
const p: Example1 = {
  x: number;
  y: string;
};
          </pre>

        <p>We can access the fields of an <span class="code">Example1</span> value called <span class="code">p</span> by writing <span class="code">p.x</span> and <span class="code">p.y</span>.</p>

    <h3>Sum types</h3>

      <p>Most recent PLs support types whose values may be <b>one</b> of multiple different types.</p>

      <p>There are different ways to do this, but all have at least two common features:
        <ul>
          <li>We can <b>construct</b> a value by bundling <b>one</b> "smaller" value.</li>
          <li>We can <b>inspect</b> a value to find out which type its "smaller" value is.</li>
        </ul>
      </p>

      <p>In PL theory, this general concept is known as a <i>sum type</i>.</p>

      <h4>Purpose</h4>

        <p>Sum types are the "or" in our data types.</p>

        <p>A sum type of <span class="code">A</span> and <span class="code">B</span> contains an <span class="code">A</span> value <b>or</b> a <span class="code">B</span> value.</p>

        <p>Specifically, we have a way to <b>branch on</b> which type it is at runtime.</p>

        <p>This includes <b>some</b> language features called "variants", "enums", and "discriminated/tagged unions".</p>

        <p>This doesn't include the <span class="code">union</span> feature in C/C++, because we can't branch on the type of a <span class="code">union</span> value.</p>

        <p>This is not directly supported in pure OOP languages like Java, but it can be simulated with inheritance.</p>

        <p>We'll see the pure OOP way of doing this stuff later in the course.</p>

      <h4>Terminology</h4>

        <p>Same history as product types: sum types are sort of vaguely like addition.</p>

        <p>If <span class="code">X</span> is a product type of <span class="code">A</span> and <span class="code">B</span>, then |<span class="code">X</span>| is |<span class="code">A</span>|+|<span class="code">B</span>|.</p>

        <p>A better name might be "OR type", but that would also get confusing to say.</p>

      <h4>In TypeScript</h4>

        <p>In TypeScript, our sum types will be implemented as <i>tagged unions</i>.</p>

        <p>This is not the only form of sum types in TypeScript, but it's the one we'll focus on.</p>

        <p>Consider a type that is "either a <span class="code">number</span> or a <span class="code">string</span>".</p>

        <p>To represent this in TypeScript, we start by defining object types for our two possibilities:</p>

        <pre class="code">
type NumberThing = {
  tag: "num",
  numValue: number
};

type StringThing = {
  tag: "str",
  strValue: string
};
        </pre>

        <p>The <span class="code">tag</span> field is key here:</p>

        <ul>
          <li>A <span class="code">NumberThing</span> value can <b>only</b> have the value <span class="code">"num"</span> in its <span class="code">tag</span> field.</li>
          <li>A <span class="code">StringThing</span> value can <b>only</b> have the value <span class="code">"str"</span> in its <span class="code">tag</span> field.</li>
        </ul>

        <p>We can use whatever tag names we want, <b>as long as they don't overlap</b>.</p>

        <p>Our "<span class="code">number</span> or <span class="code">string</span>" sum type is defined with the <span class="code">|</span> operator:</p>

        <pre class="code">
type Thing = NumberThing | StringThing;
        </pre>

        <p>This means quite literally "a <span class="code">Thing</span> is either a <span class="code">NumberThing</span> or a <span class="code">StringThing</span>".</p>

        <p>How do we <b>inspect</b> a <span class="code">Thing</span> value and discover whether it's a <span class="code">NumberThing</span> or a <span class="code">StringThing</span>?</p>

        <p>We can use a <span class="code">switch/case</span> statement over its <span class="code">tag</span>:</p>

        <pre class="code">
function example2(x: Thing): void {
  switch (x.tag) {
    case "num":
      ...; // use x.numValue
      break;

    case "str":
      ...; // use x.strValue
      break;
  }
}
        </pre>

        </p>

    <h3>Recursive types</h3>

      <p>A type called <span class="code">T</span> is <i>recursive</i> if a value of type <span class="code">T</span> can contain a "smaller" value of type <span class="code">T</span>.</p>

      <p>Review recursion from past courses!</p>

      <h4>Purpose</h4>

        <p>Recursive types allow us to represent <i>nesting</i> in our data structures.</p>

        <p>For example, an <span class="code">if</span> statement in C can contain another <span class="code">if</span> statement.</p>

        <p>This means our data type that represents <span class="code">if</span> statements must be recursive.</p>

      <h4>Terminology</h4>

        <p>In general, the term <i>recursive</i> describes something that refers to itself.</p>

        <p>A recursive type may "contain" itself, a recursive function may call itself, etc.</p>

      <h4>In TypeScript</h4>

        <p>We don't have to do anything special to define recursive types in TypeScript.</p>

        <p>A type can reference itself in its own definition.</p>

        <p>The <span class="code">List</span> type in assignment 1 is <i>indirectly</i> recursive:<br>
        its definition references <span class="code">ConsCell</span>, whose definition references <span class="code">List</span>.</p>

    <h3>Common data structures</h3>

      <h4>Arrays</h4>

        <p>An <i>array</i> is a built-in data structure with cells labeled by numeric indices.</p>

        <p>Arrays are good for <b>accessing</b> individual elements by index.</p>

        <p>In TypeScript, arrays are written with square brackets: <span class="code">[1,2,3]</span>.</p>

        <p>In many PLs arrays are fixed-size, but in TypeScript an array can change size.</p>

      <h4>Lists</h4>

        <p>A <i>(linked) list</i> is a recursive data structure where each node has <b>zero or one</b> child nodes (or "next pointers").</p>

        <ast-tree>
          <ast-node data-name="1">
            <ast-node data-name="2">
              <ast-node data-name="3"></ast-node>
            </ast-node>
          </ast-node>
        </ast-tree>

        <p>Lists are good for <b>traversing</b> all elements in order.</p>

      <h4>Trees</h4>

        <p>There are a variety of recursive data structures called <i>trees</i>.</p>

        <p>The most common difference between tree structures is in how many children each node has.</p>

        <p>For example, a <i>binary tree</i> is a recursive data structure where each node has <b>zero or two</b> child nodes.</p>

        <ast-tree>
          <ast-node data-name="1">
            <ast-node data-name="2">
              <ast-node data-name="3"></ast-node>
              <ast-node data-name="4"></ast-node>
            </ast-node>
            <ast-node data-name="5"></ast-node>
          </ast-node>
        </ast-tree>

    <h3>Abstract syntax trees</h3>

      <h4>Order of operations</h4>

        <p>Consider a complex arithmetic expression like <span class="code">1 + ((2 * 3 + 4) * 5) * 6</span>.</p>

        <p>How do we determine which operation will happen first?</p>

        <p>We have an <i>order of operations</i>: innermost parentheses first, multiplication before addition.</p>

        <p>How does a <b>program</b> determine which operation will happen first?</p>

        <p>Think about the string-processing algorithm you'd need to write...</p>

      <h4>Abstract syntax trees</h4>

        <p>To make operations like this easier, we use a specialized data structure.</p>

        <p>We could represent the expression from above with the following binary tree.</p>

        <span class="code">1 + ((2 * 3 + 4) * 5) * 6</span>

        <ast-tree>
          <ast-node data-name="+">
            <ast-node data-name="1"></ast-node>
            <ast-node data-name="*">
              <ast-node data-name="*">
                <ast-node data-name="+">
                  <ast-node data-name="*">
                    <ast-node data-name="2"></ast-node>
                    <ast-node data-name="3"></ast-node>
                  </ast-node>
                  <ast-node data-name="4"></ast-node>
                </ast-node>
                <ast-node data-name="5"></ast-node>
              </ast-node>
            <ast-node data-name="6"></ast-node>
            </ast-node>
          </ast-node>
        </ast-tree>

        <p>This is an <i>abstract syntax tree</i> (AST).</p>

        <p>This is a <i>binary</i> tree because each of our operators has exactly two arguments.</p>

        <p>In general, the number of children at each node depends on the operator it represents.</p>

      <h4>In TypeScript</h4>

        <p>We could represent the tree above as a binary tree of strings.</p>

        <pre class="code">
type AST = ASTNode | null;

type ASTNode = {
  data: string;
  left: AST;
  right: AST;
}
        </pre>

        <p>But this type can also represent <b>invalid</b> ASTs!</p>

        <p>We can be more precise:</p>

        <pre class="code">
type AST = NumLeaf | PlusNode | TimesNode;

type NumLeaf = {
  tag: "num";
  value: number;
}

type PlusNode = {
  tag: "plus";
  leftSubtree: AST;
  rightSubtree: AST;
}

type TimesNode = {
  tag: "times"
  leftSubtree: AST;
  rightSubtree: AST;
}
        </pre>

        <p>This makes it impossible to represent invalid operations in our tree.</p>

  </body>

</html>

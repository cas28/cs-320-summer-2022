<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <script src="../dist/bundle.js"></script>
    <link rel="stylesheet" href="../style/lecture.css">
    <title>Lecture 8: CS 320, Summer 2022</title>
  </head>

  <body>

    <h1>CS 320, Summer 2022</h1>
    <h2>Lecture 8: static analysis and type systems</h2>

    <h3>Introduction</h3>

      <p>Let's review our interpreter structure, and what we've seen so far.</p>

      <figure class="phases_figure">
        <table class="arrow_table">
          <tr><td class="arrow_label">bits</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
        <span class="phase">source input</span>
        <table class="arrow_table">
          <tr><td class="arrow_label">text</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
        <span class="phase">syntax analysis</span>
        <table class="arrow_table">
          <tr><td class="arrow_label">AST</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
        <span class="phase">static analysis</span>
        <table class="arrow_table">
          <tr><td class="arrow_label">AST</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
        <span class="phase">optimization</span>
        <table class="arrow_table">
          <tr><td class="arrow_label">AST</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
        <span class="phase">execution</span>
        <table class="arrow_table">
          <tr><td class="arrow_label">results</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
      </figure>

      <p>In previous weeks, we've explored <i>syntax analysis</i> with <i>regexes</i> and <i>CFGs</i>.</p>

      <p>In each assignment, we've had <i>execution</i> code, and in assignment 4 we're focusing on it.</p>

      <p>There are two phases we still haven't explored at all: <i>static analysis</i> and <i>optimization</i>.</p>

      <p>We're going to be mostly skipping <i>optimization</i>, unfortunately.</p>

      <p>It's a great subject, but it's a bit more advanced than we have time for in CS 320.</p>

      <p>Today, and in assignment 5, we'll focus on <i>static analysis</i>.</p>

      <p>The main form of static analysis we'll consider is <i>typechecking</i>.</p>

    <h3>Static and dynamic</h3>

      <p>The adjectives <i>static</i> and <i>dynamic</i> are super important to PL terminology.</p>

      <p>They have many different meanings in English, but these are the relevant ones:</p>
      <ul>
        <li><i>static</i>: "at rest"/"unmoving"</li>
        <li><i>dynamic</i>: "in motion"/"moving"</li>
      </ul>

      <p>The <i>static</i> properties of a program are observed by <b>reading</b> the program.</p>

      <p>The <i>dynamic</i> properties of a program are observed by <b>running</b> the program.</p>

      <p>We don't use this phrase in practice, but we might call our <i>execution phase</i> "dynamic analysis".</p>

    <h3>Correctness</h3>

      <p>In CS, we usually use the term <i>correct</i> in a precise technical way.</p>

      <p>A program is <i>correct</i> if it <b>always</b> behaves <b>exactly</b> as intended.</p>

      <p>What exactly is "intended" depends on context.</p>

      <h4>Specification</h4>

        <p>The <i>specification</i> (or <i>spec</i>) of a program defines its intended behavior.</p>

        <p><b>Without</b> a specification, a program <b>cannot be correct or incorrect</b>.</p>

        <p>Specifications are usually <i>informal</i>: written partly using natural language.</p>

        <p>There are many different aspects of program behavior we may want to specify.</p>

      <h4>Different kinds of correctness</h4>

        <p>Many fields within CS study some kind of "correctness" criteria.</p>

        <p>The field of <i>computational complexity</i> studies <i>efficiency</i>.</p>

        <p>The field of <i>human-computer interaction</i> studies <i>usability</i>.</p>

        <p>The field of <i>computer security</i> studies, well, <i>security</i>.</p>

        <p>These are all forms of <i>correctness</i>, among many others.</p>

        <p>Each of these fields provides tools to <i>specify</i> and <i>analyze</i> relevant forms of correctness.</p>

      <h4>Safety</h4>

        <p>In PL, we're mostly concerned with <i>safety</i>: the absence of errors.</p>

        <p>(This is much different than <i>security</i>, as a security class can explain.)</p>

        <p>The study of PL provides tools to specify and analyze the safety of a program.</p>

        <p>We include:
          <ul>
            <li><i>runtime errors</i>, when the program signals an error result on valid input</li>
            <li><i>logic errors</i>, when the program gives a "wrong" result without signaling an error</li>
          </ul>
        </p>

        <p>We consider a program <i>correct</i> if it <b>always</b> executes without errors.</p>

    <h3>Checking</h3>

      <p>How can we know whether a program is correct?</p>

      <h4>Testing</h4>

        <p>One way of <i>checking</i> a program's correctness is by <i>testing</i> it.</p>

        <p>Courses on software engineering cover testing in depth.</p>

        <p>By definition, software testing involves <b>running</b> the program.</p>

        <p>Imagine we want to check this pseudo-code for errors:</p>

        <pre class="code">
print 1;
null.f(); // null pointer error</pre>

        <p>What happens when we test the code?</p>

        <p>Well, we have to <b>run</b> the code to test it.</p>

        <p>First it prints <span class="code">1</span>, then it throws an error.</p>

        <p>Great! Our test can catch the error, and then we'll know our code is <i>incorrect</i>.</p>

        <p>This is a good result.</p>

        <p>What could go wrong with testing?</p>

      <h4>Risky tests</h4>

        <p>Okay, now imagine we're working on some code to wipe hard drives.</p>

        <p>We want to check this code for errors:</p>

        <pre class="code">
wipeAllHardDrives();
null.f();</pre>

        <p>What happens when we test the code?</p>

        <p>Well, we have to <b>run</b> the code to test it.</p>

        <p>First it wipes our hard drive, then it throws an error.</p>

        <p>Wait... we were using some of those hard drives.</p>

        <p>We know our program is <i>incorrect</i>, but at what cost?</p>

        <p>This is not so good of a result.</p>

        <p>We can solve this with <i>mocking</i>, but that takes extra work.</p>

      <h4>Slow tests</h4>

        <p>Imagine we're working on some climate modeling code that crunches petabytes of data.</p>

        <p>We want to check this code for errors:</p>

        <pre class="code">
functionThatTakesTwoWeeksToRun();
null.f();</pre>

        <p>What happens when we test the code?

        <p>Well, we have to <b>run</b> the code to test it.</p>

        <p>First it spends two weeks crunching numbers, then it throws an error.</p>

        <p>We know our program is <i>incorrect</i>, but two weeks have passed.</p>

        <p>Our boss is mad at us for wasting time on one of the most expensive computers in the world.</p>

        <p>This is not so good of a result either.</p>

      <h4>Predictive reasoning</h4>

        <p>There must be a better way!</p>

        <p>As a human, you can reason about an action without doing it, right?</p>

        <p>You don't even need all the details of the situation.</p>

        <p>For example, if I played a 1-on-1 match with any professional basketball player, I would certainly lose.</p>

        <p>I can know that without even knowing which professional player I'd be facing.</p>

      <h4>Static analysis</h4>

        <p>The purpose of <i>static analysis</i> is to predict results <b>without running</b> the program.</p>

        <p>This also means we don't have to come up with test inputs for static analysis.</p>

        <p>Static analysis is more powerful than you might expect, but it is also not perfect.</p>

        <p>Static analysis doesn't replace testing; they complement each other.</p>

        <p>In a certain sense, static analysis can be thought of as a "free test suite".</p> 

        <p>This may be misleading, though, because again: it <b>does not run</b> the program.</p>

    <h3>Typechecking</h3>

      <p>The concept of <i>static types</i> is foundational in static analysis.</p>

      <p>Knowing an expression's <i>type</i> helps us predict things about it.</p>

      <h4>Types</h4>

        <p>A <i>type</i> describes a set of expressions that have some common structure.</p>

        <p>Familiar types include integers, booleans, integer arrays, boolean arrays, ...</p>

        <p>Nearly all programming languages have a concept of a "type", but they vary widely.</p>

      <h4>Dynamic types</h4>

        <p>Our interpreter for assignment 4 throws a <b>runtime</b> error on <span class="code">1 + true</span>.</p>

        <p>This is a <i>dynamic type error</i>, <b>because</b> it happens at runtime.</p>

        <p>In assignment 4, our toy language is <i>dynamically-typed</i>: type errors <b>only</b> exist at runtime.</p>

        <p>The most common modern dynamically-typed languages are Python and JavaScript.</p>

        <p>Other examples include Ruby, Lua, Scheme, Clojure, PHP, Bash, PowerShell, ...</p>

      <h4>Static types</h4>

        <p>We can <b>predict</b> that <span class="code">1 + true</span> will fail, right?</p>

        <p>Our language forbids adding <b>any</b> number to <b>any</b> boolean.</p>

        <p>A <i>statically-typed</i> language predicts these errors by <i>typechecking</i>.</p>

      <h4>Typecheckers</h4>

        <p>A <i>typechecker</i> is a program (or algorithm) that makes these predictions.</p>

        <p>We say a language is <i>statically-typed</i> if it comes with a standard typechecker.</p>

        <p>We say a program is <i>well-typed</i> if the typechecker does not predict any type errors.</p>

        <p>The opposite is <i>ill-typed</i>, when the typechecker does predict a type error.</p>

        <p>(It sounds a little less silly in British English than in American English.)</p>

        <p>In most modern languages, typechecking is the bulk of <i>static analysis</i>.</p>

      <h4>Type safety</h4>

        <p>A statically-typed language is <i>type-safe</i> if it never throws a type-related error at runtime.</p>

        <p>An ideal typechecker guarantees type safety by catching errors <b>before</b> runtime.</p>

        <p>There are two core logical properties that relate to <i>type-safety</i>:</p>

        <ul>
          <li>A typechecker is <i>sound</i> if it never reports a false positive.</li>
          <li>A typechecker is <i>complete</i> if it never reports a false negative.</li>
        </ul>

        <p>There are varying degrees: a typechecker can be <b>partially</b> sound and complete.</p>

        <p>Almost no typecheckers are both <b>fully</b> sound and <b>fully</b> complete.</p>

        <p>(It is possible in theory, but only for relatively simple type systems and use cases.)</p>

        <p>In practice, most typecheckers prioritice <i>soundness</i> over <i>completeness</i>.</p>

        <p>TypeScript is a notable example of prioritizing <i>completeness</i> over <i>soundness</i>.</p>

      <h4>Type theory</h4>

        <p>The field of <i>type theory</i> is fundamentally a study of <i>formal logic</i>.</p>

        <p>(You might have caught on to that already.)</p>

        <p>On paper, we have some very elegant type theories with very nice logical properties.</p>

        <p>Most of these are based on <i>lambda calculus</i>, which we sadly won't have time to study in depth in this course.</p>

        <p>There are a few languages <b>fully</b> based on type theory: Agda, Coq, Lean, Idris, Nuprl, Cedille, ...</p>

        <p>These are generally designed for theoretical or experimental work, though.</p>

        <p>The concerns of applied software development tend to be a bit "messier" than formal logic.</p>

        <p>In practice, most existing languages used for "real-world" software engineering only take some inspiration from the formal study of type theory.</p>

        <p>Haskell and Scala are notable exceptions, which try to balance formal type theory with applied software engineering concerns.</p>

    <h3>Typechecking algorithms</h3>

      <p>Okay, how do we predict type errors in a program?</p>

      <p>In general, we traverse the program's AST and check the <i>typing rules</i> of our language at each step.</p>

      <p>This is really a relatively straightforward AST traversal.</p>

      <h4>Well-typed example</h4>

        <p>For example, consider this <i>well-typed</i> AST in the language from assignment 4:</p>

        <ast-tree>
          <ast-node data-name="or">
            <ast-node data-name="lessThan">
              <ast-node data-name="1"></ast-node>
              <ast-node data-name="2"></ast-node>
            </ast-node>
            <ast-node data-name="lessThan">
              <ast-node data-name="2"></ast-node>
              <ast-node data-name="1"></ast-node>
            </ast-node>
          </ast-node>
        </ast-tree>

        <p>How do we know this is well-typed? Starting from the root:</p>

        <ol>
          <li><span class="code">or</span> takes two <span class="code">bool</span>s and produces a <span class="code">bool</span>: do we have two <span class="code">bool</span>s?</li>
          <li>
            <ol>
              <li><span class="code">lessThan</span> takes two <span class="code">num</span>s and produces a <span class="code">bool</span>, so that's okay as the left operand to <span class="code">or</span>; do we have two <span class="code">num</span>s?</li>
              <li>
                <ol>
                  <li><span class="code">1</span> is a <span class="code">num</span>, so that's okay as the left operand to <span class="code">lessThan</span>.</li>
                  <li><span class="code">2</span> is a <span class="code">num</span>, so that's okay as the right operand to <span class="code">lessThan</span>.</li>
                </ol>
              </li>
            </ol>
          </li>
          <li>The right subtree proceeds the same way as the left.</li>
        </ol>

      <h4>Ill-typed example </h4>

        <p>Now consider this <i>ill-typed</i> AST in the same language:</p>

        <ast-tree>
          <ast-node data-name="or">
            <ast-node data-name="lessThan">
              <ast-node data-name="1"></ast-node>
              <ast-node data-name="true"></ast-node>
            </ast-node>
            <ast-node data-name="lessThan">
              <ast-node data-name="2"></ast-node>
              <ast-node data-name="1"></ast-node>
            </ast-node>
          </ast-node>
        </ast-tree>

        <p>How do we know this is ill-typed? Starting from the root:</p>

        <ol>
          <li><span class="code">or</span> takes two <span class="code">bool</span>s and produces a <span class="code">bool</span>: do we have two <span class="code">bool</span>s?</li>
          <li>
            <ol>
              <li><span class="code">lessThan</span> takes two <span class="code">num</span>s and produces a <span class="code">bool</span>, so that's okay as the left operand to <span class="code">or</span>; do we have two <span class="code">num</span>s?</li>
              <li>
                <ol>
                  <li><span class="code">1</span> is a <span class="code">num</span>, so that's okay as the left operand to <span class="code">lessThan</span>.</li>
                  <li class="error"><span class="code">true</span> is a <span class="code">bool</span>, so that's <b>not</b> okay as the right operand to <span class="code">lessThan</span>.</li>
                </ol>
              </li>
            </ol>
          </li>
        </ol>

        <p>We can exit as soon as we find a type error, or we can keep traversing the rest of the tree to try to report as many type errors as we can.</p>

      <h4>Typechecking variables</h4>

        <p>Things get a little more complex when we introduce <i>variables</i>.</p>

        <p>To typecheck a program with variables, we need to know the <b>type of each variable</b>.</p>

        <p>This is why languages with typecheckers don't allow you to change the type of a variable after initialization.</p>

        <p>Remember the <span class="code">Scope</span> type from our assignments?</p>

        <p>At runtime, we keep around a mapping from variable names to <i>values</i>.</p>

        <p>This allows us to interpret ASTs containing variables.</p>

        <p>During typechecking, we keep around a mapping from variable names to <i>types</i>.</p>

        <p>We'll see this code pattern in assignment 5.</p>

      <h4>Scopechecking</h4>

        <p>In a language with variables, we also usually check the <i>scope</i> of each variable.</p>

        <p>This process is called <i>scopechecking</i>, and is most often done at the same time as typechecking.</p>

        <p>Consider this code in the language of assignment 4:</p>

        <pre class="code">
x = x + 1;
declare x = 1;
print x;</pre>

        <p>This is invalid because <span class="code">x</span> is not declared before its use.</p>

        <p>Consider this code with an <span class="code">if</span> statement:</p>

        <pre class="code">
if (2 == 1 + 1) {
  declare x = 1;
  print x;
}
print x;</pre>

        <p>The second <span class="code">print</span> is invalid because <span class="code">x</span> is only in scope within the <span class="code">if</span> body.</p>

        <p>This is still true even though the <span class="code">if</span> body will always execute!</p>

        <p>We will also explore scopechecking in assignment 5.</p>

    <h3>Typing rules</h3>

      <p>We have a special way to write the rules that define a typechecker.</p>

      <p>These <i>typing rules</i> are a <b>specification</b> for our typechecker code.</p>

      <h4>Reading typing rules</h4>

        <p>Typing rules look like this:
          <deriv-tree>
            <span slot="rule">RuleName</span>
            <span slot="premise">premise1</span>
            <span slot="premise">premise2</span>
            <span slot="premise">premise3</span>
            <span slot="premise">...</span>
            <span slot="conclusion">conclusion</span>
          </deriv-tree>
        </p>

        <p>In each rule, <b>if all premises are true, then the conclusion is true</b>.</p>

        <p>If the top is empty, then the conclusion is just true.
          <deriv-tree>
            <span slot="rule">RuleName</span>
            <span slot="conclusion">conclusion</span>
          </deriv-tree>
        </p>

        <p>The syntax <span class="code">e : t</span> means "the <i>expression</i> <span class="code">e</span> has <i>type</i> <span class="code">t</span>".</p>

        <p>We call this a <i>typing judgement</i>.</p>

        <p>This is where TypeScript gets its type notation from (<span class="code">let x: number = 1</span>).</p>

      <h4>Toy language typing rules</h4>

        <p>We have two types: <span class="code">number</span> and <span class="code">boolean</span>.</p>

        <p>We'll use this naming convention in our rules:
          <ul>
            <li><span class="code">n</span> is any numeric <i>value</i></li>
            <li><span class="code">b</span> is any boolean <i>value</i></li>
            <li><span class="code">e</span>, <span class="code">e<sub>1</sub></span>, <span class="code">e<sub>2</sub></span>, and <span class="code">e<sub>3</sub></span> are any <i>expression</i></li>
            <li><span class="code">t</span>, <span class="code">t<sub>1</sub></span>, and <span class="code">t<sub>2</sub></span> are any <i>type</i></li>
          </ul>
        </p>

        <p>These aren't all of the typing rules for our toy language from the assignments, just a sample.</p>

        <p class="ruleset">
          <deriv-tree>
            <span slot="rule">Value<sup>n</sup></span>
            <span slot="conclusion">n : number</span>
          </deriv-tree>

          <deriv-tree>
            <span slot="rule">Value<sup>b</sup></span>
            <span slot="conclusion">b : boolean</span>
          </deriv-tree>

          <deriv-tree>
            <span slot="rule">Plus</span>
            <span slot="premise">e<sub>1</sub> : num</span>
            <span slot="premise">e<sub>2</sub> : num</span>
            <span slot="conclusion">(e<sub>1</sub> + e<sub>2</sub>) : num</span>
          </deriv-tree>

          <deriv-tree>
            <span slot="rule">And</span>
            <span slot="premise">e<sub>1</sub> : bool</span>
            <span slot="premise">e<sub>2</sub> : bool</span>
            <span slot="conclusion">(e<sub>1</sub> &amp;&amp; e<sub>2</sub>) : bool</span>
          </deriv-tree>

          <deriv-tree>
            <span slot="rule">LessThan</span>
            <span slot="premise">e<sub>1</sub> : num</span>
            <span slot="premise">e<sub>2</sub> : num</span>
            <span slot="conclusion">(e<sub>1</sub> &lt; e<sub>2</sub>) : bool</span>
          </deriv-tree>

          <deriv-tree>
            <span slot="rule">Conditional</span>
            <span slot="premise">e<sub>1</sub> : boolean</span>
            <span slot="premise">e<sub>2</sub> : t<sub>1</sub></span>
            <span slot="premise">e<sub>3</sub> : t<sub>2</sub></span>
            <span slot="premise">t<sub>1</sub> = t<sub>2</sub></span>
            <span slot="conclusion">(e<sub>1</sub> ? e<sub>2</sub> : e<sub>3</sub>) : t<sub>1</sub></span>
          </deriv-tree>
        </p>

        <p>Let's look at that <span class="code">Conditional</span> rule closely.</p>

        <p>Why do we require <span class="code">t<sub>1</sub> = t<sub>2</sub></span>?</p>

        <p>Consider an expression like <span class="code">(true ? 1 : false) + 2</span>.</p>

        <p>This doesn't run into an error at runtime, so it should be fine, right?</p>

        <p>What if we had runtime user input?</p>

        <pre class="code">
declare x = input bool;
print ((x ? 1 : false) + 2);</pre>

        <p>This code is safe if the user inputs <span class="code">true</span>, but unsafe if they input <span class="code">false</span>.</p>

        <p>If we want to achieve <i>soundness</i>, we have to err on the side of caution.</p>

        <p>In a language with subtyping, this rule may be relaxed: we might just require that <span class="code">t<sub>1</sub></span> and <span class="code">t<sub>2</sub></span> have a common supertype.</p>

      <h4>Typing derivations</h4>

        <p>To check a type on paper, we play a little game of <i>typing derivations</i>.</p>

        <p>This is similar to the derivations we did with CFGs, but a little different.</p>

        <p>Like with CFGs, playing this game helps us <b>debug</b> our typechecker.</p>

        <p>We start with the typing judgement we want to check under a line, for example:</p>

        <deriv-tree>
          <span slot="conclusion">((1 &lt; 2) || true) : bool</span>
        </deriv-tree>

        <p>Next we apply a rule whose bottom judgement looks like ours.</p>

        <p>Specifically, the two judgement ASTs must have the <b>same root</b>.</p>

        <deriv-tree>
          <span slot="rule">Or</span>
          <span slot="premise">(1 &lt; 2) : bool</span>
          <span slot="premise">true : bool</span>
          <span slot="conclusion">((1 &lt; 2) || true) : bool</span>
        </deriv-tree>

        <p>None of the other rules apply in this case, but sometimes we may have multiple rules to choose between.</p>

        <p>Now we have to repeat the process for both premises.</p>

        <deriv-tree>
          <span slot="rule">Or</span>
          <deriv-tree slot="premise">
            <span slot="rule">LessThan</span>
            <span slot="premise">1 : num</span>
            <span slot="premise">2 : num</span>
            <span slot="conclusion">(1 &lt; 2) : bool</span>
          </deriv-tree>
          <deriv-tree slot="premise">
            <span slot="rule">Value<sup>b</sup></span>
            <span slot="conclusion">true : bool</span>
          </deriv-tree>
          <span slot="conclusion">((1 &lt; 2) || true) : bool</span>
        </deriv-tree>

        <p>The right subtree is <b>complete</b>: the <span class="code">Value<sup>b</sup> rule has no premises.</p>

        <p>The left subtree is still <b>incomplete</b>: it has premises that we haven't given rules for.</p>

        <deriv-tree>
          <span slot="rule">Or</span>
          <deriv-tree slot="premise">
            <span slot="rule">LessThan</span>
              <deriv-tree slot="premise">
                <span slot="rule">Value<sup>n</sup></span>
                <span slot="conclusion">1 : num</span>
              </deriv-tree>
              <deriv-tree slot="premise">
                <span slot="rule">Value<sup>n</sup></span>
                <span slot="conclusion">2 : num</span>
              </deriv-tree>
            <span slot="conclusion">(1 &lt; 2) : bool</span>
          </deriv-tree>
          <deriv-tree slot="premise">
            <span slot="rule">Value<sup>b</sup></span>
            <span slot="conclusion">true : bool</span>
          </deriv-tree>
          <span slot="conclusion">((1 &lt; 2) || true) : bool</span>
        </deriv-tree>

        <p>The derivation is <b>complete</b> when all premises are under a line. This shows our original judgement to be <b>true</b>.</p>

        <p>If we find that it is <b>impossible</b> to produce a <b>complete</b> derivation, we have shown our original judgement to be <b>false</b>.</p>

        <deriv-tree>
          <span slot="rule">Or</span>
          <deriv-tree slot="premise">
            <span slot="rule">LessThan</span>
              <deriv-tree slot="premise">
                <span slot="rule">Value<sup>n</sup></span>
                <span slot="conclusion">1 : num</span>
              </deriv-tree>
              <span slot="premise" class="error">true : num</span>
            <span slot="conclusion">(1 &lt; true) : bool</span>
          </deriv-tree>
          <deriv-tree slot="premise">
            <span slot="rule">Value<sup>b</sup></span>
            <span slot="conclusion">true : bool</span>
          </deriv-tree>
          <span slot="conclusion">((1 &lt; true) || true) : bool</span>
        </deriv-tree>

      <h4>Typecheckers</h4>

        <p>Remember how a <i>parser</i> produces a <i>parse tree</i>?</p>

        <p>We can represent typing derivations in a tree data structure just as well.</p>

        <p>A <i>typechecker</i> <b>could</b> produce a <i>typing derivation</i>.</p>

        <p>In theory, we often imagine this to be the output of a typechecker.</p>

        <p>In practice, we usually only care <b>whether a derivation is possible.</b></p>

        <p>If it <b>is</b> possible to produce a valid derivation, the program is <i>well-typed</i>.</p>

        <p>If it <b>is not</b> possible to produce a valid derivation, the program is <i>ill-typed</i>.</p>

        <p>This means most typecheckers don't actually construct a tree to return.</p>

        <p>The typechecker is checking whether a derivation <b>could</b> exist, without actually building it.</p>

        <p>The standard role of a typechecker is just to discover errors!</p>

        <p>The theory of typing derivations is just the theory that our typechecking algorithms are derived from.</p>

    <h3>Looking forward</h3>

      <p>In assignment 4, we implement <i>imperative statements</i> in our toy language.</p>

      <p>This includes several <i>control flow</i> features, like conditionals (<span class="code">if</span>) and loops (<span class="code">for</span>).</p>

      <p>What's still missing before it feels like a "programming language"?</p>

      <p>Our next task will be to add <i>functions</i> to our language.</p>

      <p>We will explore both the <i>static analysis</i> and <i>execution</i> of functions.</p>

      <p>Pretty soon, we'll have a <i>procedural</i> language we can write "real programs" in!</p>

      <p>At that point, we'll turn our attention to surveying other language <i>paradigms</i>.</p>

      <p>There's only a few weeks left!</p>

  </body>

</html>

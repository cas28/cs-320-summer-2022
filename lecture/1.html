<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <script src="../dist/bundle.js"></script>
    <link rel="stylesheet" href="../style/lecture.css">
    <title>Lecture 1: CS 320, Summer 2022</title>
  </head>

  <body>
    <h1>CS 320, Summer 2022</h1>
    <h2>Lecture 1: course introduction</h2>

    <h3>About these notes</h3>

      <p>Welcome to CS 320! These lecture pages are my notes for each week's lectures. I'll present from these notes during lecture, and I hope they'll be a useful resource when preparing for upcoming lectures and reviewing recordings of past lectures.</p>

      <p>There is not a one-to-one correspondence between lectures and these pages of lecture notes; some pages of notes will take us more than one lecture to cover, and some lectures will involve demonstrations that aren't covered in the notes.</p>

      <p>To be clear, that means these notes are intentionally <b>incomplete</b> without the accompanying lectures. They're meant to help guide your study (and my focus), not to tell you absolutely everything you need to know in order to do well in this course. Make good use of the lecture recordings!</p>

    <h3>Font conventions</h3>

      <p>In course material, I use the <i>italics</i> and <b>bold</b> styles differently.</p>

      <p>I use <i>italics</i> for key terms that I'm introducing or reminding you of.</p>

      <p>I use <b>bold</b> for emphasis to make sure you don't miss some text.</p>

      <p>Both are meant to draw your attention, but for different reasons.</p>

      <p>Try to pay attention to this convention!</p>

    <h3>Orientation</h3>

      <h4>Read the syllabus!</h4>

        <p>Go to <a href="https://canvas.pdx.edu">https://canvas.pdx.edu</a> and you should be able to see the course. Let me know if you're unable to find it.</p>

        <p><b>The syllabus tells you how to interact with the course.</b></p>

        <p>If you're not sure where to find something, check the syllabus first!</p>

    <h3>Course introduction</h3>

      <h4>What is "Principles of Programming Languages"?</h4>

        <p>It's a vague course title because it's a hard field to describe quickly.</p>

        <p>This field is called <i>"programming language theory"</i> or "PLT", often shortened to simply "programming languages" or "PL".</p>

        <p>There are many interrelated disciplines in the field of PL, including:</p>
        <ul>
          <li>Programming language design: inventing and improving language features</li>
          <li>Compiler/interpreter engineering: implementing and optimizing language features</li>
          <li>Proof theory and proof engineering: defining and verifying program correctness</li>
          <li>Developer experience: UX and HCI for programming languages</li>
        </ul>

        <p>Historically, the field of PL grew out of formal logic. We'll see some evidence of this.</p>

      <h4>How does PL benefit you?</h4>

        <p>The most visible results of PL work are in our development environments.</p>

        <p>We use lots of tools that create, translate, and modify programs:
          <ul>
            <li>Compilers: GCC, rustc, tsc, javac, GHC, ...</li>
            <li>Interpreters: CPython, Node.js, GHCi, ...</li>
            <li>Metaprogramming systems: cpp, Lisp macros, Template Haskell, ...</li>
            <li>Templating systems: JSX, Mustache, ASP.NET Razor, ...</li>
            <li>Documentation generators: JavaDoc, pydoc, rustdoc, Haddock, ...</li>
            <li>Program analysis tools: JSLint, Valgrind, LiquidHaskell, ... </li>
            <li>Integrated development environments: VSCode, IntelliJ, XCode, ...</li>
          </ul>
        </p>

        <p>All of these tools have one thing in common: they deal with <i>programs as data</i>.</p>

      <h4>Why should you study PL?</h4>

        <p>Most good professional drivers know a lot about how cars work.</p>

        <p>To use complex tools effectively, you have to study them carefully.</p>

        <p>As programmers, we often like to think of computers as our fundamental tools.</p>

        <p>This isn't really accurate unless we're writing raw processor microcode by hand.</p>

        <p>In practice, <i>programming languages</i> are our fundamental tools.</p>

        <p>As a programmer, your job is to use programming languages effectively.</p>

        <p>It's helpful to know a bit about how they work!</p>

      <h4>Course staff</h4>

        <p>I'm Katie Casamento:</p>
        <ul>
          <li>email: <a href="mailto:cas28@pdx.edu">cas28@pdx.edu</a></li>
          <li>office hours: Wednesday 2-3PM, Zoom link in the syllabus</li>
        </ul>

        <p>I've been programming for about 20 years at this point, which is terrifying to realize.</p>

        <p>We'll get to reminisce about the old days later in the course.</p>

        <p>I got my MS from PSU in 2019, with a focus in PL. I've been teaching here since 2018, when I started part-time during my MS.</p>

        <p>I owe Andrew Tolmach, Mark Jones, and Jingke Li a lot of credit for helping me develop this course material!</p>

        <p>I worked with each of them as a TA in this course, and my material is heavily inspired by theirs, especially Mark's.</p>

        <p>Any errors in the material are my own, of course; at this point all of the material we'll be using was written by me.</p>

    <h3>Programs about programs</h3>

      <h4>Programs as data</h4>

        <p>To work with programs as data, we need a good <i>data structure</i>.</p>

        <p>What data structure could we use to represent this C code?
          <pre class="code">
for (int i = 0; i < 10; i += 1) {
  if (i % 2 == 0) {
    printf("%d", i * 3);
  }
}
          </pre>
        </p>

        <p>Two immediate answers:</p>

        <p>The source code is like a <span class="code">string</span>, and the executable is like a <span class="code">byte[]</span> of machine code.</p>

        <p><span class="code">cpp</span> works with the <span class="code">string</span> data, and <span class="code">valgrind</span> works with the <span class="code">byte[]</span> data.</p>

        <p>But strings and byte arrays are not great data structures for many uses!</p>

        <p>Say we want an algorithm to decide how <i>nested</i> the <span class="code">printf</span> call is: how many loops or conditionals it's inside. (The answer would be 2.)</p>

        <p>We could count curly braces or indentation...</p>

        <p>But what if we wrote the code like this?</p>

        <pre class="code">
for (int i = 0; i < 10; i += 1)
if (i % 2 == 0)
printf("%d", i * 3);
        </pre>

        <p>The answer is still 2, but there's no curly braces or indentation to count.</p>

        <p>It's still possible to solve the problem by inspecting the source text, but it's a pain.</p>

        <p>Inspecting the machine code isn't any better: we can count jumps, but we have to be very careful to only count the <b>right</b> jumps.</p>

        <p>We need a data structure that represents nesting!</p>

      <h4>Programs as trees</h4>

        <p>We need <i>trees</i>.</p>

        <p>Remember trees? (Seriously, review <i>trees</i> and <i>recursion</i>.)</p>

        <p>Consider this tree with a string at each node and leaf:</p>

        <ast-tree data-name="for">
          <ast-node data-name="=">
            <ast-node data-name="int"></ast-node>
            <ast-node data-name="i"></ast-node>
            <ast-node data-name="0"></ast-node>
          </ast-node>
          <ast-node data-name="<">
            <ast-node data-name="i"></ast-node>
            <ast-node data-name="10"></ast-node>
          </ast-node>
          <ast-node data-name="+=">
            <ast-node data-name="i"></ast-node>
            <ast-node data-name="1"></ast-node>
          </ast-node>
          <ast-node data-name="if">
            <ast-node data-name="==">
              <ast-node data-name="%">
                <ast-node data-name="i"></ast-node>
                <ast-node data-name="2"></ast-node>
              </ast-node>
                <ast-node data-name="0"></ast-node>
            </ast-node>
            <ast-node data-name="printf">
              <ast-node data-name="&quot;%d&quot;"></ast-node>
              <ast-node data-name="*">
                <ast-node data-name="i"></ast-node>
                <ast-node data-name="3"></ast-node>
              </ast-node>
            </ast-node>
          </ast-node>
        </ast-node>

        <p>This idea will be the starting point for our exploration of PL.</p>

  </body>

</html>
